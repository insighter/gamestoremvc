﻿using System.ComponentModel.DataAnnotations;

namespace GameStore.Domain.Entities
{
    public class ShippingDetails
    {
        [Required(ErrorMessage = "What is your name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Add first delivery address")]
        [Display(Name = "First address")]
        public string Line1 { get; set; }

        [Display(Name = "Secondary address")]
        public string Line2 { get; set; }

        [Display(Name = "Third address")]
        public string Line3 { get; set; }

        [Required(ErrorMessage = "Add city")]
        public string City { get; set; }

        [Required(ErrorMessage = "Add country")]
        public string Country { get; set; }

        public bool GiftWrap { get; set; }
    }
}